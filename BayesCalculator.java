import java.util.*;

/**
* This class performs simple bayes calculation by taking both fractions and decimals as input
* @author chetan garikapati
*/
public class BayesCalculator {
    public static void main(String args[]) {
      Scanner scanner = new Scanner(System.in);

      //Change this value in case repeated calculations are to performed
      int i= 1;
      while(i<2) {
          i++;
          
          System.out.println("Enter P(A): ");
          double a = parseFractions(scanner.next());          
          
          System.out.println("Enter P(B|A): ");
          double ba = parseFractions(scanner.next());
          
          System.out.println("Enter P(B): ");
          double b = parseFractions(scanner.next());          
          
          System.out.println("Pr(A|B) : " + (ba*a)/b);
      }
      
    }
    
    public static double parseFractions(String fraction) {
        if (fraction.contains("/")) {
            String[] fractions = fraction.split("/");
            double numerator = Double.parseDouble(fractions[0]);
            double denominator = Double.parseDouble(fractions[1]);
            return numerator/denominator;
        } 
        return Double.parseDouble(fraction);
    }
}
